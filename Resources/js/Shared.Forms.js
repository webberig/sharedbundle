var Shared = Shared || {};
Shared.Forms = {
    init: function()
    {
        $('input.datepicker').datepicker(
            {
                dateFormat: 'dd/mm/yy'
            }
        );
    }
}
$(Shared.Forms.init);