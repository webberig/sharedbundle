<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mathieu
 * Date: 14/02/13
 * Time: 21:46
 * To change this template use File | Settings | File Templates.
 */

namespace Webberig\SharedBundle\Twig;


class FormExtension extends \Twig_Extension
{
    /****************************************************************************************************************
     * Utilities
     ****************************************************************************************************************/
    private $env;
    public function initRuntime(\Twig_Environment $environment)
    {
        $this->env = $environment;
    }
    private function getId($name, $params)
    {
        if (isset($params["formId"]))
            $id = $params["formId"] . "_";
        else
            $id = "";
        if (isset($params["group"]))
            $id .= $params["group"] . "_";
        $id .= $name;
        return $id;
    }
    public function getName()
    {
        return 'form_extension';
    }
    private function output($template, $name, $params = array())
    {
        $id = $this->getId($name, $params);
        if (isset($params["group"])) $name = $params["group"] . "[" . $name . "]";
        return $this->env->render($template, array('name' => $name, 'id' => $id, 'params' => $params));
    }

    public function getFilters() {
        $arr = array();
        foreach ($this->controls as $key => $method)
        {
            $arr[$key] = new \Twig_Filter_Method($this, $method, array('is_safe' => array('html')));
        }
        return $arr;
    }
    public function getFunctions() {
        $arr = array();
        foreach ($this->controls as $key => $method)
        {
            $arr[$key] = new \Twig_Function_Method($this, $method, array('is_safe' => array('html')));
        }
        return $arr;
    }

    /****************************************************************************************************************
     * Definitions
     ****************************************************************************************************************/
    private $controls = array(
        "fText" => 'formText',
        "fDate" => 'formDate',
        "fPassword" => 'formPassword',
        "fSelectbox" => 'formSelectbox',
        "fTextarea" => 'formTextarea',
        'fCheckbox' => 'formCheckbox'
    );

    /****************************************************************************************************************
     * Controls
     ****************************************************************************************************************/
    public function formSelectbox($name, $params = array())
    {
        return $this->output('WebberigSharedBundle:Forms:selectbox.html.twig', $name, $params);
    }
    public function formText($name, $params = array())
    {
        return $this->output('WebberigSharedBundle:Forms:text.html.twig', $name, $params);
    }
    public function formDate($name, $params = array())
    {
        if (isset($params["value"]) && $params["value"] instanceof \DateTime)
        {
            $params["value"] = $params["value"]->format("d/m/Y");
        }
        return $this->output('WebberigSharedBundle:Forms:date.html.twig', $name, $params);
    }
    public function formPassword($name, $params = array())
    {
        return $this->output('WebberigSharedBundle:Forms:password.html.twig', $name, $params);
    }
    public function formCheckbox($name, $params = array())
    {
        return $this->output('WebberigSharedBundle:Forms:checkbox.html.twig', $name, $params);
    }
    public function formTextarea($name, $params = array())
    {
        return $this->output('WebberigSharedBundle:Forms:textarea.html.twig', $name, $params);
    }
}