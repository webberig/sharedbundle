<?php
namespace Webberig\SharedBundle\Utility;

class ArrayUtil
{
    /**
    * @param array|\Traversable $data
    * @return bool True, als er met een foreach() over $data kan worden gegaan
    */
    public static function isTraversable($data)
    {
    return is_array($data) || $data instanceof \Traversable;
    }

    /**
    * @param array|\ArrayAccess $data
    * @return bool True, als array keys/indexes ([]) gebruikt kunnen worden op $data
    */
    public static function isArrayType($data)
    {
    return is_array($data) || $data instanceof \ArrayAccess;
    }

}